# -*- encoding: UTF-8 -*-
""" Say `My {Body_part} is touched` when receiving a touch event
"""
ip = "192.168.1.102"
port = 9559

import sys
import time
import argparse
import motion
import almath

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule

# Global variable to store the ReactToTouch module instance
ReactToTouch = None
memory = None


class Test(ALModule):
    def __init__(self, name):
        ALModule.__init__(self, name)

        self.pp = ALProxy("ALRobotPosture", ip, port)
        # self.pp.goToPosture("LyingBelly", 0.667)
        self.pp.goToPosture("StandInit", 0.667)
        self.tts = ALProxy("ALTextToSpeech")
        self.tts.setVolume(1)
        self.tts.setLanguage("English")
       #self.tts.say("Hello Sparky")
        self.mp = ALProxy("ALMotion", ip, port)
        self.mp.rest()
       # kiss(self)
        

class ReactToTouch(ALModule):
    """" A simple module able to react
        to touch events.
    """

    def __init__(self, name):
        ALModule.__init__(self, name)
        # No need for IP and port here because
        # we have our Python broker connected to NAOqi broker

        # Create a proxy to ALTextToSpeech for later use
        self.tts = ALProxy("ALTextToSpeech")
        self.tts.setVolume(1)
        self.tts.setLanguage("English")
        self.pp = ALProxy("ALRobotPosture", ip, port)
        self.mp = ALProxy("ALMotion", ip, port)

        self.pp.goToPosture("Sit", 0.667)
        # Subscribe to TouchChanged event:
        global memory
        memory = ALProxy("ALMemory")
        memory.subscribeToEvent("TouchChanged",
                                "ReactToTouch",
                                "onTouched")

    def onTouched(self, strVarName, value):
        """ This will be called each time a touch
       is detected.

        """

        # Unsubscribe to the event when talking,
        # to avoid repetitions
        memory.unsubscribeToEvent("TouchChanged",
                                  "ReactToTouch")

        touched_bodies = []
        print '!'
        print value
        for p in value:
            if p[1]:
                touched_bodies.append(p[0])
        yaw = 0.5  # Arbitrary value within range
        pitch = 0.5
        speed = 0.5
        joints = ["HeadYaw", "HeadPitch"]  # Names of the joints (string list)
        # angles = [yaw , pitch] # list of the angles (in radians)
        print touched_bodies
        self.mp.setStiffnesses(joints, 0.8)
        for body in touched_bodies:
            if ('Head/Touch/' in body):
                self.mp.setAngles(joints, [0.0, 0.0], speed)
                self.monologue()
                # self.dance()
            elif (body[0] == 'R'):
                yaw = -0.8
                if ('RFoot' in body):
                    pitch = 0.8
                    self.mp.setAngles(joints, [yaw, pitch], speed)
                    self.tts.say("Wat is het toch lekker weer")
                elif ('RHand' in body):
                    pitch = 0.3
                    self.mp.setAngles(joints, [yaw, pitch], speed)
                    self.tts.say("Hallo jongeman")
            elif (body[0] == 'L'):
                yaw = 0.8
                if ('LFoot' in body):
                    pitch = 0.8
                    self.mp.setAngles(joints, [yaw, pitch], speed)
                    self.tts.say("Ik heb het onwijs naar mijn zin")
                elif ('LHand' in body):
                    pitch = 0.3
                    self.mp.setAngles(joints, [yaw, pitch], speed)
                    self.tts.say("Hoi Loes")
                    #  else:
                    #    self.say(touched_bodies)

        # self.say(touched_bodies)
        # self.tts.say("Burp")

        # Subscribe again to the event
        memory.subscribeToEvent("TouchChanged",
                                "ReactToTouch",
                                "onTouched")
        """
    def say(self, bodies):
        if (bodies == []):
            return

        sentence = "My " + bodies[0]

        for b in bodies[1:]:
            sentence = sentence + " and my " + b

        if (len(bodies) > 1):
            sentence = sentence + " are"
        else:
            sentence = sentence + " is"
        sentence = sentence + " touched."

        self.tts.say(sentence)
        """

def kiss(self):
    # Wake up robot
    self.mp.wakeUp()

    useSensorValues = False

    frame = motion.FRAME_ROBOT

    # pathRArm
    pathRArm = []
    currentTf = self.mp.getTransform("RArm", frame, useSensorValues)

    # 1
    target1Tf = almath.Transform(currentTf)
    # SPELEN MET DEZE WAARDES: hand bij mond
    target1Tf.r2_c4 += 0.8  # y
    target1Tf.r3_c4 += 0.4  # z

    # 2
    target2Tf = almath.Transform(currentTf)
    # SPELEN MET DEZE WAARDES: hand vooruit
    target2Tf.r2_c4 -= 0.5  # y
    target2Tf.r3_c4 -= 0.7  # z

    pathRArm.append(list(target1Tf.toVector()))
    pathRArm.append(list(target2Tf.toVector()))

    coef       = 1.5
    timesList  = [ coef*(i+1.0) for i in range(12)],  # for "LArm" in seconds
                      # [coef*(i+1) for i in range(6)] ] # for "RArm" in seconds

    targetCoordinateList = [
    [ +0.12, +0.00*coef, +0.00], # target 0
    [ +0.12, +0.00*coef, -0.10], # target 1
    [ +0.12, +0.05*coef, -0.10], # target 1
    [ +0.12, +0.05*coef, +0.10], # target 2
    [ +0.12, -0.10*coef, +0.10], # target 3
    [ +0.12, -0.10*coef, -0.10], # target 4
    [ +0.12, +0.00*coef, -0.10], # target 5
    [ +0.12, +0.00*coef, +0.00], # target 6
    [ +0.00, +0.00*coef, +0.00], # target 7
    ]
    
    # called cartesian interpolation
    self.mp.transformInterpolations("LArm", frame, targetCoordinateList, almath.AXIS_MASK_VEL, timesList)
    self.tts.say("KISS!")

    # Deactivate whole body
    isEnabled = False
    self.mp.wbEnable(isEnabled)

    # Send robot to Pose Init
   # self.pp.goToPosture("Sit", 0.667)

    # Go to rest position
    self.mp.rest()

    # end script

def monologue(self):
    self.tts.say("Hey! You look like someone who should study Artificial Intelligence.")  # Let me dance for you.")

    # Wake up robot
    self.mp.wakeUp()

    # Send robot to Stand Init
    self.pp.goToPosture("StandInit", 0.667)

    # end go to Stand Init, begin initialize whole body

    # Enable Whole Body Balancer
    isEnabled = True
    self.mp.wbEnable(isEnabled)

    # Legs are constrained fixed
    stateName = "Fixed"
    supportLeg = "Legs"
    self.mp.wbFootState(stateName, supportLeg)

    # Constraint Balance Motion
    isEnable = True
    supportLeg = "Legs"
    self.mp.wbEnableBalanceConstraint(isEnable, supportLeg)

    # end initialize whole body, define arms motions

    useSensorValues = False

    # Arms motion
    effectorList = ["LArm", "RArm"]

    frame = motion.FRAME_ROBOT

    # pathLArm
    pathLArm = []
    currentTf = self.mp.getTransform("LArm", frame, useSensorValues)

    # 1
    target1Tf = almath.Transform(currentTf)
    # SPELEN MET DEZE WAARDES
    target1Tf.r2_c4 += 0.08  # y
    target1Tf.r3_c4 += 0.14  # z

    # 2
    target2Tf = almath.Transform(currentTf)
    # SPELEN MET DEZE WAARDES
    target2Tf.r2_c4 -= 0.05  # y
    target2Tf.r3_c4 -= 0.07  # z

    pathLArm.append(list(target1Tf.toVector()))
    pathLArm.append(list(target2Tf.toVector()))
    # pathLArm.append(list(target1Tf.toVector()))
    # pathLArm.append(list(target2Tf.toVector()))
    # pathLArm.append(list(target1Tf.toVector()))

    # pathRArm
    pathRArm = []
    currentTf = self.mp.getTransform("RArm", frame, useSensorValues)
    # 1
    target1Tf = almath.Transform(currentTf)
    # SPELEN MET DEZE WAARDES
    target1Tf.r2_c4 += 0.05  # y
    target1Tf.r3_c4 -= 0.07  # z

    # # 2
    # target2Tf = almath.Transform(currentTf)
    # target2Tf.r2_c4 -= 0.08  # y
    # target2Tf.r3_c4 += 0.14  # z

    pathRArm.append(list(target1Tf.toVector()))
    # pathRArm.append(list(target2Tf.toVector()))
    # pathRArm.append(list(target1Tf.toVector()))
    # pathRArm.append(list(target2Tf.toVector()))
    # pathRArm.append(list(target1Tf.toVector()))
    # pathRArm.append(list(target2Tf.toVector()))

    pathList = [pathLArm, pathRArm]

    axisMaskList = [almath.AXIS_MASK_VEL,  # for "LArm"
                    almath.AXIS_MASK_VEL]  # for "RArm"

    # coef = 1.5

    self.mp.closeHand("RHand")
    self.mp.post.openHand("RHand")

    # called cartesian interpolation
    self.mp.post.transformInterpolations(effectorList, frame, pathList, axisMaskList,1)
    self.tts.say("Hello. My name is %s. I have over 10.000 brothers and sisters all over the world.", self.name)

    self.mp.post.transformInterpolations(effectorList, frame, pathList, axisMaskList, 1)
    self.tts.say("Some interesting and motivating stuff")

    # end define arms motions, define torso motion

    # Deactivate whole body
    isEnabled = False
    self.mp.wbEnable(isEnabled)

    # Send robot to Pose Init
    self.pp.goToPosture("Sit", 0.667)

    # Go to rest position
    self.mp.rest()

    # end script


def main(ip, port):
    """ Main entry point
    """
    # We need this broker to be able to construct
    # NAOqi modules and subscribe to other modules
    # The broker must stay alive until the program exists
    myBroker = ALBroker("myBroker",
                        "0.0.0.0",  # listen to anyone
                        0,  # find a free port and use it
                        ip,  # parent broker IP
                        port)  # parent broker port

    # """
    # global ReactToTouch
    # ReactToTouch = ReactToTouch("ReactToTouch")
# """
    global Test
    Test = Test("Test")
# """
    try:
        while True:
            time.sleep(1)
            # val = memory.getData("FaceDetected", 0)
            # print val
    except KeyboardInterrupt:
        print
        print "Interrupted by user, shutting down"
        myBroker.shutdown()
        sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.1.102",
                        help="Robot ip address")
    parser.add_argument("--port", type=int, default=9559,
                        help="Robot port number")
    args = parser.parse_args()
    main(args.ip, args.port)

